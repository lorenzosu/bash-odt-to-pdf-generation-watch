Bash script using watch and sha1sum to automatically generate a new pdf file for
odt files in a directory whenever they change (default is watch every 2 seconds + 1 second sleep).

The idea is that you are editing one or more ODT files and wanting to generate a PDF asap, without manually saving, ovewriting the file.

ODT is used in LibreOffice, OpenOffice and (possibly, untested gDrive)

I put this in a known scripts path and just run it in the directory of interest.

As of now, you will need to brutally terminate the script with 1 or 2 SIGTERMS

Tested on Linux with LibreOffice.
should work elsewhere as long as bash, watch and sha1sum are installed
